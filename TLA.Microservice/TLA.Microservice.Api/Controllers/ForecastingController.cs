﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TLA.Microservice.Application.DTOs;
using TLA.Microservice.Application.Interfaces;

namespace TLA.Microservice.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ForecastingController : ControllerBase
    {
        private readonly ILogger<ForecastingController> _logger;
        private readonly ILeadPreferencesService _leadPreferencesService;

        /// <summary>
        /// Initializes a new instance of <see cref="ForecastingController"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="leadPreferencesService"></param>
        /// <param name="userService"></param>
        public ForecastingController(ILogger<ForecastingController> logger, 
            ILeadPreferencesService leadPreferencesService)
        {
            _logger = logger;
            _leadPreferencesService = leadPreferencesService;
        }

        /// <summary>
        /// Gets all leads preferences
        /// </summary>
        /// <returns>The list of lead preferences</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IList<LeadsPreferenceDto>>> GetLeadsPreferences()
        {
            try
            {
                var leads = await _leadPreferencesService.GetAllLeadPreferences();
                if (leads.Any())
                {
                    return Ok(leads);
                }

                return NotFound();
            }
            catch (Exception e)
            {
                _logger.LogError($"An exception happened on {nameof(GetLeadsPreferences)}.", e);
                return BadRequest("Something wrong happened during your request!");
            }
        }

        /// <summary>
        /// Create a new lead preference through data gathering from UI
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<DataGatheringDto>> CreateLeadsPreferences([FromBody] LeadsPreferenceDto leadsPreference)
        {
            try
            {
                var leads = await _leadPreferencesService.CreateNewLeadPreferences(leadsPreference);
                return CreatedAtAction(nameof(CreateLeadsPreferences),null, leadsPreference);
            }
            catch (Exception e)
            {
                _logger.LogError($"An exception happened on {nameof(CreateLeadsPreferences)}.", e);
                return BadRequest("Something wrong happened during your request!");
            }
        }
    }
}
