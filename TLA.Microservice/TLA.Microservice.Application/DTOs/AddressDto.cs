﻿namespace TLA.Microservice.Application.DTOs
{
    /// <summary>
    /// The address dto class
    /// </summary>
    public class AddressDto
    {
        /// <summary>
        /// The street name
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// The zip code number
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// The locality
        /// </summary>
        public string Locality { get; set; }
    }
}