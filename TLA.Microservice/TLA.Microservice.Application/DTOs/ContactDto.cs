﻿using System.Collections.Generic;

namespace TLA.Microservice.Application.DTOs
{
    public class ContactDto
    {
        /// <summary>
        /// The phone number
        /// </summary>
        public PhoneDto Phone { get; set; }

        /// <summary>
        /// The email address
        /// </summary>
        public string Email { get; set; }
    }
}