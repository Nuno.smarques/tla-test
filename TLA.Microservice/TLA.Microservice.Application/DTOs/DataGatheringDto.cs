﻿namespace TLA.Microservice.Application.DTOs
{
    public class DataGatheringDto
    {
        /// <summary>
        /// The user data
        /// </summary>
        public UserDto User { get; set; }

        /// <summary>
        /// The lead preferences information
        /// </summary>
        public LeadsPreferenceDto Lead { get; set; }
    }
}