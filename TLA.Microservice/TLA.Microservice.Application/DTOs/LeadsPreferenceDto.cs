﻿using System.Collections.Generic;
using TLA.Microservice.Domain.Entities;

namespace TLA.Microservice.Application.DTOs
{
    /// <summary>
    /// The lead preferences dto
    /// </summary>
    public class LeadsPreferenceDto
    {
        public string VehicleType { get; set; }

        public string VehicleStyle { get; set; }

        public List<VehicleMakerDto> VehicleMaker { get; set; }

        public PriceRangeDto PriceRange { get; set; }

        public UserDto User { get; set; }
    }
}