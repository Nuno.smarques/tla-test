﻿namespace TLA.Microservice.Application.DTOs
{
    public class PhoneDto
    {
        public string CountryCode { get; set; }
        public string Number { get; set; }
    }
}