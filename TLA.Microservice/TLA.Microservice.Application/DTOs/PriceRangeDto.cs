﻿namespace TLA.Microservice.Application.DTOs
{
    public class PriceRangeDto
    {
        /// <summary>
        /// The minimum price
        /// </summary>
        public int Minimum { get; set; }

        /// <summary>
        /// The maximum price
        /// </summary>
        public int Maximum { get; set; }
    }
}