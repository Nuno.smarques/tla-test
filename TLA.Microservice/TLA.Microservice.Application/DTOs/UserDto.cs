﻿using System;

namespace TLA.Microservice.Application.DTOs
{
    /// <summary>
    /// The user data transfer object
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// The name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// The age in years
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// The gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// The address
        /// </summary>
        public AddressDto Address { get; set; }

        /// <summary>
        /// The contacts
        /// </summary>
        public ContactDto Contacts { get; set; }
    }
}