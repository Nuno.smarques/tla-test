﻿namespace TLA.Microservice.Application.DTOs
{
    public class VehicleMakerDto
    {
        public string Name { get; set; }
    }
}