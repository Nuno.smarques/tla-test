﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TLA.Microservice.Application.DTOs;

namespace TLA.Microservice.Application.Interfaces
{
    /// <summary>
    /// Lead preferences service interface
    /// </summary>
    public interface ILeadPreferencesService
    {
        /// <summary>
        /// Get all lead preferences.
        /// </summary>
        /// <returns></returns>
        Task<IList<LeadsPreferenceDto>> GetAllLeadPreferences();

        /// <summary>
        /// Creates a new lead preference.
        /// </summary>
        /// <param name="leadPreference"></param>
        /// <returns>Creates a new lead preference</returns>
        Task<LeadsPreferenceDto> CreateNewLeadPreferences(LeadsPreferenceDto leadPreference);
    }
}