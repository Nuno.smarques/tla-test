﻿using Microsoft.Extensions.DependencyInjection;
using TLA.Microservice.Application.Interfaces;
using TLA.Microservice.Application.Mapping;
using TLA.Microservice.Application.Services;

namespace TLA.Microservice.Application
{
    public static class IoCServices
    {

        /// <summary>
        /// Add all the services we are using
        /// </summary>
        /// <param name="services"></param>
        public static void AddServices(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AutomapperProfile));
            services.AddScoped<ILeadPreferencesService, LeadPreferencesService>();
        }
    }
}