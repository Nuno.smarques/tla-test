﻿using AutoMapper;
using System.Linq;
using System.Runtime.CompilerServices;
using TLA.Microservice.Application.DTOs;
using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Infrastructure.Repositories;

namespace TLA.Microservice.Application.Mapping
{
    /// <summary>
    /// Automapper profile class
    /// </summary>
    public class AutomapperProfile : Profile
    {
        /// <summary>
        /// Create mapping between entities and dto's
        /// </summary>
        public AutomapperProfile()
        {
            CreateMap<LeadsPreference, LeadsPreferenceDto>()
                .ForMember(dto => dto.VehicleMaker,
                    opt =>
                        opt.MapFrom(x =>
                            x.LeadsVehicleMakerChoices.Select(y => y.VehicleMaker)
                                .ToList()));
            CreateMap<LeadsPreferenceDto, LeadsPreference>()
                .ForMember(dto => dto.LeadsVehicleMakerChoices,
                    opt =>
                        opt.MapFrom(x =>
                            x.VehicleMaker.Select(y => y).ToList())
                );

            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Address, AddressDto>().ReverseMap();
            CreateMap<Contact, ContactDto>().ReverseMap();
            CreateMap<Phone, PhoneDto>().ReverseMap();
            CreateMap<VehicleMaker, VehicleMakerDto>().ReverseMap();

            CreateMap<LeadsVehicleMakerChoice, VehicleMakerDto>()
                .ForPath(vm => vm.Name,
                            opt => opt.MapFrom(lvmc => lvmc.VehicleMaker.Name)
                    ).ReverseMap();

            CreateMap<PriceRange, PriceRangeDto>().ReverseMap();
        }
    }
}