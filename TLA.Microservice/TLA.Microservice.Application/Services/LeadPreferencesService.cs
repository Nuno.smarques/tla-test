﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TLA.Microservice.Application.DTOs;
using TLA.Microservice.Application.Interfaces;
using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Infrastructure.Repositories;

namespace TLA.Microservice.Application.Services
{
    /// <summary>
    /// Lead preference service
    /// </summary>
    public class LeadPreferencesService : ILeadPreferencesService
    {
        private readonly ILogger<LeadPreferencesService> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of <see cref="LeadPreferencesService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        public LeadPreferencesService(ILogger<LeadPreferencesService> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<IList<LeadsPreferenceDto>> GetAllLeadPreferences()
        {
            try
            {
                var leadPreferences = await _unitOfWork.LeadPreferences.GetAll();

                 var leadDto = _mapper.Map<IList<LeadsPreferenceDto>>(leadPreferences);
                 return leadDto;
            }
            catch (DbException ex)
            {
                _logger.LogError("A database error occured when tried to retrieve leads from database!", ex);
                throw new DBConcurrencyException($"A database error occured when tried to retrieve leads from database! {ex}");
            }
            catch (Exception ex)
            {
                _logger.LogError("A general error occured when tried to retrieve leads from database!", ex);
                throw new Exception($"A general error occured when tried to retrieve leads from database! {ex}");
            }
        }

        /// <inheritdoc />
        public async Task<LeadsPreferenceDto> CreateNewLeadPreferences(LeadsPreferenceDto leadsPreference)
        {
            try
            {
                var leadEntity = _mapper.Map<LeadsPreference>(leadsPreference);
                var result =  await _unitOfWork.LeadPreferences.Add(leadEntity);
                await _unitOfWork.Commit();
                return _mapper.Map<LeadsPreferenceDto>(result);
            }
            catch (DbException ex)
            {
                _unitOfWork.Rollback();
                _logger.LogError("A database error occured when tried to retrieve leads from database!", ex);
                throw new DBConcurrencyException($"A database error occured when tried to retrieve leads from database! {ex}");
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                _logger.LogError("A general error occured when tried to retrieve leads from database!", ex);
                throw new Exception($"A general error occured when tried to retrieve leads from database! {ex}");
            }
        }
    }
}