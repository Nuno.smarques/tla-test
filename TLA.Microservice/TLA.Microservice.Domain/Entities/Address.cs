﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public class Address : BaseEntity
    {
        public Address()
        {
            Users = new HashSet<User>();
        }

        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string Locality { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
