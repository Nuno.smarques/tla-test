﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public class Contact : BaseEntity
    {
        public Contact()
        {
            Users = new HashSet<User>();
        }

        public string Email { get; set; }
        public Guid PhoneId { get; set; }

        public virtual Phone Phone { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
