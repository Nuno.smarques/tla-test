﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public partial class LeadsPreference : BaseEntity
    {
        public LeadsPreference()
        {
            LeadsVehicleMakerChoices = new HashSet<LeadsVehicleMakerChoice>();
        }

        public string VehicleType { get; set; }
        public string VehicleStyle { get; set; }
        public Guid UserId { get; set; }
        public Guid PriceRangeId { get; set; }

        public virtual PriceRange PriceRange { get; set; }
        public User User { get; set; }
        public virtual ICollection<LeadsVehicleMakerChoice> LeadsVehicleMakerChoices { get; set; }
    }
}
