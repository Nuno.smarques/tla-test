﻿using System;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public class LeadsVehicleMakerChoice : BaseEntity
    {
        public Guid VehicleMakerId { get; set; }
        public Guid LeadPreferenceId { get; set; }

        public virtual LeadsPreference LeadPreference { get; set; }
        public virtual VehicleMaker VehicleMaker { get; set; }
    }
}
