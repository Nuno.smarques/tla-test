﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public partial class Phone : BaseEntity
    {
        public Phone()
        {
            Contacts = new HashSet<Contact>();
        }

        public string CountryCode { get; set; }
        public string Number { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
