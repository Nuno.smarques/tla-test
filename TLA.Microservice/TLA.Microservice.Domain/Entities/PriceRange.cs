﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public class PriceRange : BaseEntity
    {
        public PriceRange()
        {
            LeadsPreferences = new HashSet<LeadsPreference>();
        }

        public int? Minimum { get; set; }
        public int? Maximum { get; set; }

        public virtual ICollection<LeadsPreference> LeadsPreferences { get; set; }
    }
}
