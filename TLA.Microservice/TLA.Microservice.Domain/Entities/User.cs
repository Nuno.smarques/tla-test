﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            LeadsPreferences = new HashSet<LeadsPreference>();
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthdate { get; set; }
        public string Gender { get; set; }
        public Guid ContactsId { get; set; }
        public Guid AddressId { get; set; }

        public virtual Address Address { get; set; }
        public virtual Contact Contacts { get; set; }
        public virtual ICollection<LeadsPreference> LeadsPreferences { get; set; }
    }
}
