﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TLA.Microservice.Domain.Entities
{
    public class VehicleMaker : BaseEntity
    {
        public VehicleMaker()
        {
            LeadsVehicleMakerChoices = new HashSet<LeadsVehicleMakerChoice>();
        }

        public string Name { get; set; }

        public virtual ICollection<LeadsVehicleMakerChoice> LeadsVehicleMakerChoices { get; set; }
    }
}
