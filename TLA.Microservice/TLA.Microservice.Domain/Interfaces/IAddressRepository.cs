﻿using TLA.Microservice.Domain.Entities;

namespace TLA.Microservice.Domain.Interfaces
{
    public interface IAddressRepository : IRepository<Address>
    {
        
    }
}