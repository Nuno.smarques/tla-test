﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TLA.Microservice.Domain.Entities;

namespace TLA.Microservice.Domain.Interfaces
{
    /// <summary>
    /// The lead preferences interface
    /// </summary>
    public interface ILeadPreferencesRepository : IRepository<LeadsPreference>
    {
    }
}