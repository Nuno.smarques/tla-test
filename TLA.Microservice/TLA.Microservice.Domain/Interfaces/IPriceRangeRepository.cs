﻿using TLA.Microservice.Domain.Entities;

namespace TLA.Microservice.Domain.Interfaces
{
    public interface IPriceRangeRepository : IRepository<PriceRange>
    {
        
    }
}