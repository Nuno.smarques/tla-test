﻿using TLA.Microservice.Domain.Entities;

namespace TLA.Microservice.Domain.Interfaces
{
    /// <summary>
    /// The user repository interface
    /// </summary>
    public interface IUsersRepository : IRepository<User>
    {
    }
}