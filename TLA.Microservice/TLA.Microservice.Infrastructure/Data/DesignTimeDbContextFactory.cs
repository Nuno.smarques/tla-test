﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace TLA.Microservice.Infrastructure.Data
{
    /// <summary>
    /// Provides access to configurations and initialize db context during migrations
    /// </summary>
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<TLAApplicationContext>
    {
        /// <summary>
        /// Initializes database context.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public TLAApplicationContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../TLA.Microservice.Api/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<TLAApplicationContext>();
            var connectionString = configuration.GetConnectionString("Default");
            builder.UseSqlServer(connectionString);
            return new TLAApplicationContext(builder.Options);
        }
    }
}