﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using TLA.Microservice.Domain.Entities;


namespace TLA.Microservice.Infrastructure.Data
{
    /// <summary>
    /// Database context class
    /// </summary>
    public class TLAApplicationContext : DbContext
    {
        /// <summary>
        /// Initialize a new instance of <see cref="TLAApplicationContext"/> class.
        /// </summary>
        /// <param name="options">Database options</param>
        public TLAApplicationContext(DbContextOptions<TLAApplicationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<LeadsPreference> LeadsPreferences { get; set; }
        public virtual DbSet<LeadsVehicleMakerChoice> LeadsVehicleMakerChoices { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<PriceRange> PriceRanges { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<VehicleMaker> VehicleMakers { get; set; }


        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Locality).HasMaxLength(80);

                entity.Property(e => e.Street).HasMaxLength(350);

                entity.Property(e => e.ZipCode).HasMaxLength(15);
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.HasOne(d => d.Phone)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.PhoneId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Contacts__Phone");
            });

            modelBuilder.Entity<LeadsPreference>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.VehicleStyle)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.VehicleType)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.HasOne(d => d.PriceRange)
                    .WithMany(p => p.LeadsPreferences)
                    .HasForeignKey(d => d.PriceRangeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LeadsPreferences__PriceRange");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.LeadsPreferences)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LeadsPreferences__User");
            });

            modelBuilder.Entity<LeadsVehicleMakerChoice>(entity =>
            {
                entity.ToTable("LeadsVehicleMakerChoice");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.LeadPreference)
                    .WithMany(p => p.LeadsVehicleMakerChoices)
                    .HasForeignKey(d => d.LeadPreferenceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LeadsVehicleMaker__LeadPreferences");

                entity.HasOne(d => d.VehicleMaker)
                    .WithMany(p => p.LeadsVehicleMakerChoices)
                    .HasForeignKey(d => d.VehicleMakerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LeadsVehicleMaker__VehicleMaker");
            });

            modelBuilder.Entity<Phone>(entity =>
            {
                entity.ToTable("Phone");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CountryCode).HasMaxLength(5);

                entity.Property(e => e.Number).HasMaxLength(15);
            });

            modelBuilder.Entity<PriceRange>(entity =>
            {
                entity.ToTable("PriceRange");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Birthdate).HasColumnType("date");

                entity.Property(e => e.Gender).HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__User__AddressId");

                entity.HasOne(d => d.Contacts)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.ContactsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__User__ContactsId");
            });

            modelBuilder.Entity<VehicleMaker>(entity =>
            {
                entity.ToTable("VehicleMaker");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            });
        }
    }
}
