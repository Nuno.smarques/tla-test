﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;
using TLA.Microservice.Infrastructure.Repositories;

namespace TLA.Microservice.Infrastructure.IoC
{
    public static class IoCDatabase
    {
        /// <summary>
        /// Add all the repositories we are using
        /// </summary>
        /// <param name="services"></param>
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<ILeadPreferencesRepository, LeadPreferenceRepository>();
            services.AddScoped<ILeadVehicleChoiceRepository, LeadVehicleChoiceRepository>();
            services.AddScoped<IPhoneRepository, PhoneRepository>();
            services.AddScoped<IPriceRangeRepository, PriceRangeRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IVehicleMakerRepository, VehicleMakerRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        /// <summary>
        /// Add application db context to pipeline 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static IServiceCollection AddAppContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<TLAApplicationContext>(options =>
            {
                options.UseSqlServer(connectionString);
                options.EnableSensitiveDataLogging();
            });
            return services;
        }

        /// <summary>
        /// Runs migration to ensure that we will have a db ready to work
        /// </summary>
        /// <param name="app"></param>
        public static void RunMigrations(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetRequiredService<TLAApplicationContext>();
            context.Database.EnsureCreated();
        }
    }
}