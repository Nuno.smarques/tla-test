﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TLA.Microservice.Infrastructure.Migrations
{
    public partial class CorrectSurnameColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Surame",
                table: "User",
                newName: "Surname");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Surname",
                table: "User",
                newName: "Surame");
        }
    }
}
