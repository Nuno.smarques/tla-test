﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class AddressRepository : Repository<Address>, IAddressRepository
    {
        /// <inheritdoc />
        public AddressRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}