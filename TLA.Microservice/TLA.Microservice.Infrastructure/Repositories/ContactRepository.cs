﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class ContactRepository : Repository<Contact>, IContactRepository
    {
        /// <inheritdoc />
        public ContactRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}