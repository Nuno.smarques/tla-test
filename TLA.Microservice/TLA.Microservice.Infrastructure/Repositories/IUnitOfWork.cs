﻿using System;
using System.Threading.Tasks;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <summary>
    /// Unit of work interface
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        public IVehicleMakerRepository VehicleMaker { get; set; }
        public IAddressRepository Address { get; set; }
        public IContactRepository Contact { get; set; }
        public ILeadPreferencesRepository LeadPreferences { get; set; }
        public ILeadVehicleChoiceRepository LeadVehicleMakerChoice { get; set; }
        public IPhoneRepository Phone { get; set; }
        public IPriceRangeRepository PriceRange { get; set; }
        public IUsersRepository User { get; set; }

        /// <summary>
        /// Commit all changes that happened during the transaction
        /// </summary>
        Task Commit();

        /// <summary>
        /// Rollback all changes that happened during the transaction
        /// </summary>
        void Rollback();
    }
}