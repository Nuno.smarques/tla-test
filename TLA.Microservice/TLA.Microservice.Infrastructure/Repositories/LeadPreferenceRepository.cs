﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class LeadPreferenceRepository : Repository<LeadsPreference>, ILeadPreferencesRepository
    {
        /// <inheritdoc />
        public LeadPreferenceRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Get all lead preferences with all children included
        /// </summary>
        /// <returns>A list of lead preferences</returns>
        public override async Task<IList<LeadsPreference>> GetAll()
        {
            return await _dbContext.LeadsPreferences
                                    .Include(lp => lp.User)
                                        .ThenInclude(u => u.Address)
                                    .Include(lp => lp.User)
                                        .ThenInclude(u => u.Contacts)
                                            .ThenInclude(c => c.Phone)
                                    .Include(lp => lp.PriceRange)
                                    .Include(lp => lp.LeadsVehicleMakerChoices)
                                        .ThenInclude(lvmc => lvmc.VehicleMaker)
                                    .ToListAsync();
        }
    }
}