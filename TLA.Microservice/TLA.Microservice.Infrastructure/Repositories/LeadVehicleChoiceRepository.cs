﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class LeadVehicleChoiceRepository : Repository<LeadsVehicleMakerChoice>, ILeadVehicleChoiceRepository
    {
        /// <inheritdoc />
        public LeadVehicleChoiceRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}