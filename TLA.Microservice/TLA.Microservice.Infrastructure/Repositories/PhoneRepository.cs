﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class PhoneRepository : Repository<Phone>, IPhoneRepository
    {
        /// <inheritdoc />
        public PhoneRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}