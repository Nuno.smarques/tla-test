﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class PriceRangeRepository : Repository<PriceRange>, IPriceRangeRepository
    {
        /// <inheritdoc />
        public PriceRangeRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}