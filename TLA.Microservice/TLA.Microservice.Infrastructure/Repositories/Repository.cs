﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <summary>
    /// Implementation of repository interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected TLAApplicationContext _dbContext;
        internal DbSet<T> _dbSet;

        /// <summary>
        /// Initializes a new instance of <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="dbContext"></param>
        public Repository(TLAApplicationContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<T>();
        }

        /// <inheritdoc />
        public virtual async Task<IList<T>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        /// <inheritdoc />
        public async Task<T> GetById(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        /// <inheritdoc />
        public virtual async Task<T> Add(T entity)
        {
            await _dbSet.AddAsync(entity);
            return entity;
        }

        /// <inheritdoc />
        public virtual async Task<bool> Delete(Guid id)
        {
            var entity = await GetById(id);
            Delete(entity);
            return true;
        }

        /// <inheritdoc />
        public virtual void Delete(T entityToDelete)
        {
            if (_dbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }

        /// <inheritdoc />
        public virtual T Update(T entity)
        {
            _dbSet.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        /// <inheritdoc />
        public virtual async Task<IList<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }
    }
}