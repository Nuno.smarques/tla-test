﻿using System;
using System.Threading.Tasks;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc />
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TLAApplicationContext _dbContext;
        private bool _disposed = false;
        public IVehicleMakerRepository VehicleMaker { get; set; }
        public IAddressRepository Address { get; set; }
        public IContactRepository Contact { get; set; }
        public ILeadPreferencesRepository LeadPreferences { get; set; }
        public ILeadVehicleChoiceRepository LeadVehicleMakerChoice { get; set; }
        public IPhoneRepository Phone { get; set; }
        public IPriceRangeRepository PriceRange { get; set; }
        public IUsersRepository User { get; set; }

        /// <summary>
        /// Initializes a new instance of <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="dbContext"></param>
        public UnitOfWork(TLAApplicationContext dbContext, 
            IVehicleMakerRepository vehicleMaker, 
            IAddressRepository address, 
            IContactRepository contact, 
            ILeadPreferencesRepository leadPreferences, 
            ILeadVehicleChoiceRepository leadVehicleMakerChoice, 
            IPhoneRepository phone, 
            IPriceRangeRepository priceRange, 
            IUsersRepository user)
        {
            _dbContext = dbContext;
            VehicleMaker = vehicleMaker;
            Address = address;
            Contact = contact;
            LeadPreferences = leadPreferences;
            LeadVehicleMakerChoice = leadVehicleMakerChoice;
            Phone = phone;
            PriceRange = priceRange;
            User = user;
        }

        /// <inheritdoc />
        public async Task Commit()
        {
            await _dbContext.SaveChangesAsync();
        }

        /// <inheritdoc />
        public void Rollback()
        {
            Dispose(true);
        }

        /// <summary>
        /// Dispose with a condition
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this._disposed = true;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}