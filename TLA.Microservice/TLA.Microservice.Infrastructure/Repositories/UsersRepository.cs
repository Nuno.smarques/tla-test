﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class UsersRepository : Repository<User>, IUsersRepository
    {
        /// <inheritdoc />
        public UsersRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}