﻿using TLA.Microservice.Domain.Entities;
using TLA.Microservice.Domain.Interfaces;
using TLA.Microservice.Infrastructure.Data;

namespace TLA.Microservice.Infrastructure.Repositories
{
    /// <inheritdoc cref="Repository{T}"/> />
    public class VehicleMakerRepository : Repository<VehicleMaker>, IVehicleMakerRepository
    {
        /// <inheritdoc />
        public VehicleMakerRepository(TLAApplicationContext dbContext) : base(dbContext)
        {
        }
    }
}