﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TLA.Application.Services;
using TLA.Domain.DTOs;

namespace TLA.Api.Controllers
{
    /// <summary>
    /// The gathering information controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class GatheringController : ControllerBase
    {

        private readonly ILogger<GatheringController> _logger;
        private readonly ILeadPreferencesService _leadPreferencesService;

        /// <summary>
        /// GatheringController constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="dataGatheringService">The data gathering service.</param>
        /// <param name="leadPreferencesService">The lead preferences service.</param>
        public GatheringController(ILogger<GatheringController> logger,
            ILeadPreferencesService leadPreferencesService)
        {
            _logger = logger;
            _leadPreferencesService = leadPreferencesService;
        }

        /// <summary>
        /// Get all leads available
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IList<LeadsPreferenceDto>>> LeadsPreferences()
        {
            var response = await _leadPreferencesService.Get();
            _logger.LogInformation($"The request to {nameof(LeadsPreferences)} was executed with success!");
            return Ok(response);
        }
        /// <summary>
        /// Get specific leads by filter
        /// </summary>
        /// <returns></returns>
        [HttpGet("leadsPreferencesByBrand")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IList<LeadsPreferenceDto>>> LeadsPreferencesByBrand(string brand)
        {
            try
            {
                var response = await _leadPreferencesService.Get();
                if (response is null || response.Count(l => l.VehicleMaker != null) <= 0)
                {
                    return NotFound("No results from your request.");
                }

                var vehicleMarkers = response.Where(lead => lead.VehicleMaker.Any(vm=> vm.Name == brand)).ToList();
                _logger.LogInformation($"The request to {nameof(LeadsPreferencesByBrand)} was executed with success!");
                return Ok(vehicleMarkers);
            }
            catch (Exception e)
            {
                _logger.LogError("Something went wront when we tried to request Leads from the microservice.", e);
                return BadRequest($"Something wrong happened during your request! {e.Message}");
            }
        }

        /// <summary>
        /// Create a new lead
        /// </summary>
        /// <param name="leadPreferences">The data from the interaction with ui</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GatheringUserData([FromBody] LeadsPreferenceDto leadsPreference)
        {
            try
            {
                var response = await _leadPreferencesService.Post(leadsPreference);
                _logger.LogInformation($"The request to {nameof(GatheringUserData)} was executed with success!");
                return CreatedAtAction(nameof(GatheringUserData), null, response);
            }
            catch (Exception e)
            {
                _logger.LogError("Something went wrong when we tried to insert new data gathering through the microservice.", e);
                return BadRequest(e.Message);
            }
        }
    }
}
