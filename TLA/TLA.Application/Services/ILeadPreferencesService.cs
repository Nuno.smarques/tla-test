﻿using TLA.Domain.DTOs;

namespace TLA.Application.Services
{
    /// <summary>
    /// Lead preferences web api service interface.
    /// </summary>
    public interface ILeadPreferencesService : IWebApiService<LeadsPreferenceDto, ILeadPreferencesService>
    {
    }
}