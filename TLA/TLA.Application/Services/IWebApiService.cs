﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TLA.Application.Services
{
    /// <summary>
    /// Interface to consume a web API
    /// </summary>
    public interface IWebApiService<TEntity, TClass> 
        where TEntity : class
        where TClass : class
    {
        /// <summary>
        /// Gets a list of objects from type T.
        /// </summary>
        Task<IList<TEntity>> Get();

        /// <summary>
        /// Creates a new entry from type T.
        /// </summary>
        /// <param name="data">The data to be posted from type T.</param>
        Task<TEntity> Post(TEntity data);
    }
}