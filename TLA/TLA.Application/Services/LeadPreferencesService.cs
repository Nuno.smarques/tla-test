﻿using Microsoft.Extensions.Logging;
using TLA.Domain.DTOs;

namespace TLA.Application.Services
{
    /// <summary>
    /// Lead preferences web api service implementation.
    /// </summary>
    public class LeadPreferencesService : WebApiService<LeadsPreferenceDto, LeadPreferencesService>, ILeadPreferencesService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPreferencesService"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public LeadPreferencesService(ILogger<WebApiService<LeadsPreferenceDto, LeadPreferencesService>> logger) 
            : base(logger)
        {
            ENDPOINT = "http://localhost:5000/Forecasting";
        }
    }
}