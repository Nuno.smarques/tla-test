﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace TLA.Application.Services
{
    /// <summary>
    /// Generic web api service implementation.
    /// </summary>
    /// <typeparam name="TEntity">The entity.</typeparam>
    /// <typeparam name="TClass">The class itself.</typeparam>
    public class WebApiService<TEntity, TClass> : IWebApiService<TEntity, TClass>
        where TEntity : class
        where TClass : class
    {
        /// <summary>
        /// The web api endpoint
        /// </summary>
        public string ENDPOINT { private get; set; }

        private readonly ILogger<WebApiService<TEntity, TClass>> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiService{T}"/> class.
        /// </summary>
        /// <param name="logger"></param>
        protected WebApiService(ILogger<WebApiService<TEntity, TClass>> logger)
        {
            _logger = logger;
        }

        /// <inheritdoc />
        public async Task<IList<TEntity>> Get()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using var response = await httpClient.GetAsync(ENDPOINT);
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        _logger.LogInformation( "Data retrieved with success!");
                        return JsonConvert.DeserializeObject<IList<TEntity>>(apiResponse);
                    }
                }

                _logger.LogWarning( "Something went wrong on during request, we couldn't get the data!");
                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError( "Failed to execute the GET request.", ex);
                throw new Exception($"Failed to execute the request. {ex.Message}");
            }
        }

        /// <inheritdoc />
        public async Task<TEntity> Post(TEntity data)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using var response = await httpClient.PostAsJsonAsync($"{ENDPOINT}", data);
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        _logger.LogInformation( "Data saved with success!");
                        return JsonConvert.DeserializeObject<TEntity>(apiResponse);
                    }
                }

                _logger.LogWarning( "Something went wrong on during request, data was not saved!");
                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError( "Failed to execute the POST request.", ex);
                throw new Exception($"Failed to execute the request. {ex.Message}");
            }
        }
    }
}