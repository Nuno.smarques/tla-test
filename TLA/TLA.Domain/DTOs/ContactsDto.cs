﻿using System.Collections.Generic;

namespace TLA.Domain.DTOs
{
    /// <summary>
    /// The contact dto object that contains contacts
    /// </summary>
    public class ContactsDto
    {
        /// <summary>
        /// The phone number
        /// </summary>
        public PhoneDto Phone { get; set; }

        /// <summary>
        /// The email address
        /// </summary>
        public string Email { get; set; }
    }
}