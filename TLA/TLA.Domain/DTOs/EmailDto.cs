﻿namespace TLA.Domain.DTOs
{
    /// <summary>
    /// Email dto object
    /// </summary>
    public class EmailDto
    {
        /// <summary>
        /// The email domain.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// The email address.
        /// </summary>
        public string Address { get; set; }
    }
}