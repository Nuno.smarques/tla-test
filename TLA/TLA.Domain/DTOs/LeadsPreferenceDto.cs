﻿using System.Collections.Generic;

namespace TLA.Domain.DTOs
{
    /// <summary>
    /// The lead preferences dto
    /// </summary>
    public class LeadsPreferenceDto
    {
        /// <summary>
        /// The type of vehicle
        /// </summary>
        public string VehicleType { get; set; }

        /// <summary>
        /// The style of a vehicle
        /// </summary>
        public string VehicleStyle { get; set; }

        /// <summary>
        /// List of vehicle makers
        /// </summary>
        public List<VehicleMakerDto> VehicleMaker { get; set; }

        /// <summary>
        /// The range of price to buy a vehicle of the specified kind
        /// </summary>
        public PriceRangeDto PriceRange { get; set; }


        public UserDto User { get; set; }
    }
}