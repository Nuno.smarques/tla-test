﻿namespace TLA.Domain.DTOs
{
    /// <summary>
    /// The phone dto.
    /// </summary>
    public class PhoneDto
    {
        /// <summary>
        /// The country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// The number.
        /// </summary>
        public string Number { get; set; }
    }
}