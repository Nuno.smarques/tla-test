﻿namespace TLA.Domain.DTOs
{
    /// <summary>
    /// The min and max amounts of a price
    /// </summary>
    public class PriceRangeDto
    {
        /// <summary>
        /// The minimum price.
        /// </summary>
        public int Minimum { get; set; }

        /// <summary>
        /// The maximum price.
        /// </summary>
        public int Maximum { get; set; }
    }
}