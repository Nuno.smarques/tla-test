﻿using System;

namespace TLA.Domain.DTOs
{
    /// <summary>
    /// The user dto.
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// The name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// The age in years
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// The gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// The address
        /// </summary>
        public AddressDto Address { get; set; }

        /// <summary>
        /// The contacts
        /// </summary>
        public ContactsDto Contacts { get; set; }
    }
}