﻿namespace TLA.Domain.DTOs
{
    /// <summary>
    /// Vehicle maker
    /// </summary>
    public class VehicleMakerDto
    {
        public string Name { get; set; }
    }
}